from django.urls import path

from . import views

urlpatterns = [
    path('submit-feedback', views.SubmitFeedback.as_view(), name='submit-feedback'),
    path('', views.main, name='main')
]