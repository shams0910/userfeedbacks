from django.db import models

# Create your models here.

BLANK_NULL = {'blank': True, 'null': True}


class Feedback(models.Model):
    text = models.TextField()
    name = models.CharField(max_length=255, help_text='Full name')
    email = models.EmailField()
    file = models.FileField(**BLANK_NULL)
    created_at = models.DateTimeField(auto_now_add=True, null=True)