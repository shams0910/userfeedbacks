from django.shortcuts import render, redirect
from django.views import View

from .forms import FeedbackForm
# Create your views here.


class SubmitFeedback(View):
    def get(self, request):
        return render(request, 'pages/submit-feedback.html')

    def post(self, request):
        context = {}
        feedback_form = FeedbackForm(request.POST, request.FILES)
        if feedback_form.is_valid():
            feedback_form.save()
            return render(request, 'pages/thank-you.html')
        else:
            context.update({'errors': feedback_form.errors})
            return render(request, 'pages/submit-feedback.html', context)


def main(request):
    return render(request, 'pages/home.html')
